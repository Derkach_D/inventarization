<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style>
    body{
        background-color: #dddddd;
    }
   a:link{
       color:green;
   }
   a:visited{
       color: purple;
   }
        .frame{
            border-style:solid;
            border-color: black;
            width: 120px;
            height:120px;
            padding-left: 3%;
            padding-top: 3%;
            border-spacing: 7px;
        }
        .top{
            margin-top:10%;
        }

        
    </style>
    <title>Menu</title>
</head>
<body>
<?php 
$user = 'u20341';
$pass = '3227715';
$db = new PDO('mysql:host=localhost;dbname=u20341', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$stmt = $db->prepare("SELECT * FROM Adress WHERE Adress_id = ?");
            $stmt->execute(array($_GET['adress']));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

?>
    <h1>Меню</h1>
    <br>
    <div class="row">
    <div class="col-2"></div>
        <div class="frame col-3">
            <a>Комнаты</a>
            <form action="rooms.php" id="room" method="GET">
            <?php 
            $stmt=$db->query("SELECT * FROM rooms");
            $cases=$db->query("SELECT * FROM Containers");
            ?>
                <select name="room">
                <?php 
                foreach ($stmt as $go){ ?>
                    <option value=<?php echo $go['room_id'] ?>><?php echo $go['name']?></option>
                   <?php 
                }  
                    ?>
                    </select>
            </form>
        </div>
        <div class="frame col-3">
            <a>Хранилища</a>
            <form action="containers.php" id="container" method="GET">
                <select name="container">
                <?php foreach($cases as $case){ ?>
                    <option value=<?php echo $case['container_id'] ?>><?php echo $case['name'] ?></option>
                <?php } ?>
                </select>
            </form>
        </div>
        <div class="frame col-3">
            <a>Все предметы</a>
        </div>
    </div>
    <div class="row">
        <div class="col-2"></div>
        <div class="col-3"><input type="submit" form="room" value="Перейти"></div>
        <div class="col-3"><input type="submit" form="container" value="Перейти"></div>
        <div class="col-3"><button><a href="all_items.php">Перейти</a></button></div>
    </div>
    <div class="row top">
        <div class="col-2"></div>
        <div class="col-3"><button><a href="add_thing.php">Добавить предмет</a></button></div>
        <div class="col-3"><button><a href="moderat.php">Перейти на страницу модератора</a></button></div>
        <div class="col-3"><button><a href="main.php">Вернуться на главную</a></button></div>
    </div>
    <div class="row top">
        <div class="col-2"></div>
        <div class="col-3"><button><a href="rooms_id.php">Список комнат</a></button></div>
        <div class="col-3"><button><a href="container_id.php">Список хранилищ</a></button></div>
        <div class="col-3"><button><a href="main.php">Вернуться на главную</a></button></div>
    </div>
</body>
</html>