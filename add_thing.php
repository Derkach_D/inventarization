<!DOCTYPE html>
<html lang="en">
<?php 
$user='u20341';
$pass='3227715';
$db = new PDO('mysql:host=localhost;dbname=u20341', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$adress = $db->query("SELECT * FROM Adress");
$containers = $db->query("SELECT * FROM Containers");
$owners = $db->query("SELECT * FROM Owner");
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style>
        div{
            padding-top: 1%;
        }
        body{
            background-color: #dddddd;
        }
    </style>
    <title>Добавление предмета</title>
</head>
<body>
<div class = "row">
        <div class="col-4"></div>      
        <div class="col-4"><h3>Добавить предмет:</h3> </div>
        <div class="col-4"><button><a href="main.php">Вернуться на главную страницу</a></button></div>
    </div>

<hr>
<form method="POST" action="messages.php">
<input type="hidden" name="add_thing">
    <div class = "row">
        <div class="col-1"></div>
        <div class="col-3"><a>Название:</a> </div>        
        <div class="col-4"><textarea name="thing_name" value = ""></textarea></div>
        <div class="col-4"></div>
    </div>
    <hr>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-3"><a>Добавить по адресу...</a></div>
        <div class = "col-4">
            <select name="adress">
                        <?php 
                        foreach ($adress as $adres){ ?>
                            <option value=<?php echo $adres['Adress_id'] ?>>
                            <?php echo 'Ул. '.$adres['Street'].' Дом '.$adres['House_number'].' Квартира '.$adres['Flat']?></option>
                        <?php 
                        }  
                            ?> 
            </select>
        </div>
        <div class = "col-4"></div>
    </div>

    <hr>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-3"><a>Добавить в хранилище...</a> </div>
        <div class="col-4">
            <select name="container">
                    <?php 
                    foreach ($containers as $container){ ?>
                        <option value=<?php echo $container['container_id'] ?>><?php echo $container['name']?></option>
                    <?php 
                    } ?> 
            </select>    
        </div>
        <div class = "col-4"></div>
     </div>
     <hr>
    <div class="row">
        <div class = "col-1"></div>
        <div class = "col-3"><a>Владелец</a></div>
        <div class="col-4">
                    <select name = "owner">
                        <?php foreach ($owners as $owner){ ?>
                            <option value = <?php echo $owner['Owner_id']?>><?php echo $owner['Second_name'].' '.$owner['Name'].' '.$owner['Otchestvo']?>
                            </option>
                    <?php } ?>
                    </select>
        </div>
        <div class = "col-4"></div>
    </div>

    <div class = "row">
        <div class = "col-4"></div>
        <div class = "col-4"><input type = "submit" value = "Добавить"></div>
        <div class = "col-4"></div>
    </div>
    </form>
 
    
</body>
</html>