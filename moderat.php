<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style>
    body{
        background-color: #dddddd;
    }
    textarea{
        height: 30px;
    }
    .down{
        padding-top: 5%;
        height: 100px;
    }
    .hov:hover{
        background-color: #aaa;
    }
    </style>
    <title>Модератор</title>
</head>
<body>
    <div class="row">
        <div class="col-4"></div>
        <div class="col-5"><h1>Страница модератора</h1></div>
        <div class="col-1"></div>
    </div>
    <hr>
      <div class="hov">
        <form method="POST"  action = "messages.php">
        <input type="hidden" name="add_room" value="1" />
    
        <h3>Добавить комнату:</h3>
        <br>
        <div class="row">
        <div class="col-1"></div>
        <div class="col-3"><a>Название:</a> </div>
        <div class="col-3"></div>
        <div class="col-3"></div>
        <div class="col-2"></div>
        </div>
        <div class="row">
        <div class="col-1"></div>
        <div class="col-3"><textarea name="room_name" value = ""></textarea> </div>
        <div class="col-3"><input type = "submit" value = "Добавить"> </div>
        <div class="col-3"> </div>
        <div class="col-2"> </div>
        </div>
        </form>
         <hr>
    </div>
    
   
    <div class="hov">
     <h3>Добавить хранилище</h3>
    <form method = "POST" action = "messages.php">
    <input type="hidden" name="add_container">
    <div class="row">
    <div class="col-1"></div>
    <div class="col-3"><a>Название хранилища</a> </div>
    <div class="col-3"> <a>id_Комнаты</a></div>
    <div class="col-3"> </div>
    <div class="col-2"> </div>
    </div>
    
    <div class="row">
    <div class="col-1"></div>
    <div class="col-3"><textarea name = "container_name" value = ""></textarea> </div>
    <div class="col-3"><textarea name = "room_id" value = ""></textarea> </div>
    <div class="col-3"> <input type="submit" value = "Добавить"></div>
    <div class="col-2"> <button><a href = "rooms_id.php">Список комнат</a></button></div>
    </div>
    </form>
    <hr>
    </div>
   
    
    <div class="hov">
        <h3>Добавить владельца</h3>
        <form method = "POST"  action = "messages.php">
        <input type="hidden" name = "add_owner">
        <div class="row">
        <div class="col-1"></div>
        <div class="col-3"><a>Имя</a> </div>
        <div class="col-3"> <a>Фамилия</a></div>
        <div class="col-3"><a>Отчество</a> </div>
        <div class="col-2"> </div>
        </div>

        <div class="row">
        <div class="col-1"></div>
        <div class="col-3"><textarea name="owner_name" value=""></textarea> </div>
        <div class="col-3"> <textarea name="owner_sn" value = ""></textarea> </div>
        <div class="col-3"><textarea name="otchestvo" value = ""></textarea> </div>
        <div class="col-2"><input type = "submit" value = "Добавить"></div>
        </div>
        </form>
        <hr>
    </div>
   
    <div class="hov">
        <h3>Добавить адрес</h3>
        <form method = "POST" action = "messages.php">
        <input type="hidden" name = "add_adress">
        <div class="row">
        <div class="col-1"></div>
        <div class="col-3"><a>Улица</a> </div>
        <div class="col-3"><a>Дом</a> </div>
        <div class="col-3"><a>Квартира</a></div>
        <div class="col-2"> </div>
        </div>

        <div class="row">
        <div class="col-1"></div>
        <div class="col-3"><textarea name = "street" value = ""></textarea> </div>
        <div class="col-3"><textarea name = "house" value = ""></textarea> </div>
        <div class="col-3"><textarea name = "flat" value = ""></textarea> </div>
        <div class="col-2"><input type = "submit" value = "Добавить"></div>
        </div>
        </form>
    </div>
    
    <div class="down">
    <button><a href="main.php">Вернуться на главную страницу</a></button>
    </div>
</body>
</html>