<!DOCTYPE html>
<html lang="ru">
<?php 
$user = 'u20341';
$pass = '3227715';
$db = new PDO('mysql:host=localhost;dbname=u20341', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$stmt=$db->query("SELECT * FROM Thing");
$things=$stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Список всех предметов</title>
    <style>
     body{
        background-color: #dddddd;
    }
    table {width: 100%; border-collapse: collapse;}
table thead tr {color: #ffffff; font-weight: bold; background: #00bf80;}
table thead tr td {border: 1px solid #01ab73;}
table tbody tr td {border: 1px solid #e8e9eb;}
table tbody tr:nth-child(2n) {background: #f4f4f4;}
table tbody tr:hover {background: #ebffe8;}
.tabhead {background-color:#899d5c !important;}
    </style>
   
</head>
<body>
<button><a href="main.php">Вернуться на главную</a></button>


    <table>
    <th colspan="6" class="thead">Список всех предметов</th>
    <tr class="tabhead">
        <td> id предмета </td>
        <td> Предмет </td>
        <td> Хранилище </td>
        <td> Владелец </td>
        <td> Дата добавления </td>
        <td> Удаление </td>
    </tr>
    <?php foreach ($things as $thing){ ?> 
        <tr>
        <td> <?php echo $thing['Thing_id'] ?> </td>
        <td> <?php echo $thing['Name'] ?> </td>
        <td> <?php $container_name=$db->prepare(
            "SELECT name 
            FROM Containers
            WHERE container_id = (
            SELECT container_id
            FROM Acts
            WHERE Thing_id = ?)");
            $container_name->execute(array($thing['Thing_id'])); 
            $c_n = $container_name->fetch(PDO::FETCH_ASSOC);
            echo $c_n['name'];
            ?></td>
        <td> <?php $owner_name=$db->prepare(
        'SELECT name 
        FROM Owner
        WHERE Owner_id = 
        (Select Owner_id 
        from Thing 
        where Thing_id=?)'); 
        $owner_name->execute(array($thing['Thing_id']));
        $o_n = $owner_name->fetch(PDO::FETCH_ASSOC);
        echo $o_n['name'];
        ?></td>
        <td> <?php 
        $date=$db->prepare(
            'SELECT Date 
            from Prikaz 
            where Prikaz_id =
            (select Prikaz_id 
            from Acts 
            where Thing_id = ?)');
            $date->execute(array($thing['Thing_id']));
            $d = $date->fetch(PDO::FETCH_ASSOC);
            echo $d['Date'];
        ?> </td>
        <td> <button>Удаление </button> </td>
        </tr>
                 <?php  }  ?>
    </table>
</body>
</html>